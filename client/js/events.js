"use strict",

Template.form.events({
        'submit .add-new-post': function (event) {
            event.preventDefault();

            var postImage = event.currentTarget.children[2].children[0].children[1].files[0];

            var name = event.currentTarget.children[0].value;

            var message = event.currentTarget.children[1].value;

            if (postImage == "") {
                Materialize.toast("Add an image", 4000);
            }

            if (name == "") {
                Materialize.toast("Add a name", 4000);
            }

            if (message == "") {
                Materialize.toast("Add a message", 4000);
            }

            Collections.Images.insert(postImage, function (error, fileObject) {
                if (error) {
                    //to-do: inform user that it failed.
                } else {
                    Collections.user.insert({
                        name: name,
                        createdAt: new Date(),
                        imageId: fileObject._id,
                        message: message
                    });

                    $('.grid').masonry('reloadItems');
                }
            });
        }
    }),

    Template.message.events({
        'click .delete-task': function (event) {
            Collection.message.remove({
                _id: this._id
            });
        },
    });