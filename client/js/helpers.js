"use strict",
Template.posts.helpers({
    postList: function () {
        return Collections.user.find({}, {
            sort: {
                createdAt: -1
            }
        });
    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                gutter: 15,
                columnWidth: '.grid-sizer',
                percentPosition: true
            });
        });
    },

    displayImages: function (imageId) {
        var image = Collections.Images.find({
            _id: imageId
        }).fetch();
        return image[0].url();
    } 

});