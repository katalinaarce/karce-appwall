Collections = {};

var imageStore = new FS.Store.GridFS("images");

Collections.user = new Mongo.Collection("user");

Collections.Images = new FS.Collection("images", {
    stores: [imageStore]
});

